function Chat(conf) {
	if(typeof conf != "object" || conf == null) {
		conf = {};
	}
	
	var base = typeof conf.url == "string" ? conf.url : "https://board.asm32.info";
	var event_url = base + "/!chat_events";
	var post_url = base + "/!chat";
	var emoji_uer = base + "/templates/mobile/_images/chatemoticons/";
	
	var sys_log = $("#" + (typeof conf.syslog == "string" ? conf.syslog : "syslog"));
	var chat_log = $("#" + (typeof conf.chatlog == "string" ? conf.chatlog : "chatlog"));
	
	var user_line = $("#" + (typeof conf.username == "string" ? conf.username : "chat_user"));
	var edit_line = $("#" + (typeof conf.message == "string" ? conf.message : "chat_message"));
	
	var toggle_syslog = $("#" + (typeof conf.toggle_syslog == "string" ? conf.toggle_syslog : "toggle_syslog"));
	
	var connect = false;
	var source = null;
	
	var postData = function(url, data) {
		if(!connect) {
			console.log("Not Connected !!!");
			return;
		}
		
		$.post(url, data);
	}
	
	var sendMessage = function() {
		postData(post_url, {
			cmd: "message",
			chat_message: $(edit_line).val()
		});
		
		$(edit_line).val("");
	}
	
	var userRename = function() {
		postData(post_url, {
			cmd: "rename",
			username: $(user_line).val()
		});
	}
	
	var userStatusChange = function(status) {
		postData(post_url, {
			cmd: "status",
			status: status
		});
	}
	
	var keyPressed = function(event) {
		if (event.keyCode == '13') {
			if($(this).is(edit_line)) {
				sendMessage();
			}
			else if($(this).is(user_line)) {
				userRename();
			}
		};
	}
	
	var focus = function() {
		$(toggle_controls).prop('checked', $(this).is(edit_line));
	}
	
	var insertNick = function() {
		var txt = $(edit_line).val();
		txt = '@' + $(this).text() + ': ' + txt;
		$(edit_line).val(txt);
		
		$(toggle_syslog).prop('checked', false);
		$(edit_line).focus();
	}
	
	var createUserElement = function(el, name, originalname) {
		var obj = document.createElement(el);
		$(obj).text(name);
		$(obj).addClass(originalname == name ? "user" : "fake_user");
		$(obj).attr("title", originalname);
		$(obj).click(insertNick);
		
		return obj;
	}
	
	var replaceEmoticons = function (text) {
		var emoticons = {
			':LOL:': 'rofl.gif',
			':lol:': 'rofl.gif',
			':ЛОЛ:': 'rofl.gif',
			':лол:': 'rofl.gif',
			':-)' : 'smile.gif',
			':)'  : 'smile.gif',
			':-D' : 'lol.gif',
			':D'  : 'lol.gif',
			':-Д' : 'lol.gif',
			':Д'  : 'lol.gif',
			'&gt;:-(': 'angry.gif',
			'&gt;:(' : 'angry.gif',
			':-(' : 'sad.gif',
			':('  : 'sad.gif',
			':`-(': 'cry.gif',
			':`(' : 'cry.gif',
			':\'-(': 'cry.gif',
			':\'(':  'cry.gif',
			';-)' : 'wink.gif',
			';)'  : 'wink.gif',
			':-P' : 'tongue.gif',
			':P'  : 'tongue.gif',
			':-П' : 'tongue.gif',
			':П'  : 'tongue.gif'
		};
		
		var patterns = [];
		var metachars = /[[\]{}()*+?.\\|^$\-,&#\s]/g;
		
		// build a regex pattern for each defined property
		for (var i in emoticons) {
			if (emoticons.hasOwnProperty(i)) { // escape metacharacters
				patterns.push('('+i.replace(metachars, "\\$&")+')');
			}
		}
		
		// build the regular expression and replace
		return text.replace(new RegExp(patterns.join('|'),'g'), function (match) {
			return typeof emoticons[match] != 'undefined' ? '<img src="' + emoji_uer + emoticons[match] + '" alt="' + match + '" >' : match;
		});
	}
	
	var linkify = function(txt) {
		var replacedText, replacePattern;
		//URLs starting with http://, https://, or ftp://
		replacePattern = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
		
		replacedText = txt.replace(replacePattern, '<a href="$1" target="_blank">$1</a>');
		return replacedText;
	}
	
	var tmp = {		
		connect: function() {
			source = new EventSource(event_url);
			
			source.onopen = function(event) {
				connect = true;
			}
			
			source.onerror = function(event) {
				userStatusChange(2);
			}
			
			source.onmessage = function(event) {
				//console.log("onmessage: " + event.data);
			}
			
			source.addEventListener("message", function(event) {
				var msgset = JSON.parse(event.data);
				
				$(msgset.msgs).each(function(index, element) {
					if($("#chat" + element.id).length > 0) {
						console.log("message #chat" + element.id + " exists");
						return;
					}
					
					var date = new Date(element.time * 1000);
					var hours = "0" + date.getHours();
					var minutes = "0" + date.getMinutes();
					var seconds = "0" + date.getSeconds();
					var strTime = hours.substr(-2) + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
					
					var user = createUserElement("span", element.user, element.originalname);
					
					var msg = linkify(element.text);
					msg = replaceEmoticons(msg);
					
					var p = document.createElement("p");
					$(p).attr("id", "chat" + element.id);
					$(p).append("(" + strTime + ") ");
					$(p).append(user);
					$(p).append(": " + msg);
					
					$(chat_log).append(p);
					$(chat_log).scrollTop($(chat_log)[0].scrollHeight);
				});
			});
			
			source.addEventListener("users_online", function(event) {
				var msgset = JSON.parse(event.data);
				$(sys_log).empty();
				
				$(msgset.users).each(function(index, element) {
					if(element.flagSelf) {
						$(user_line).val(element.user);
					}
					
					var p = createUserElement("p", element.user, element.originalname);
					$(sys_log).append(p);
				});
			});
		},
		
		disconnect: function() {
			userStatusChange(0);
			
			source.close();
			source = null;
			
			connect = false;
		}
	}
	
	$(user_line).keypress(keyPressed);
	$(user_line).blur(userRename);
	$(user_line).focus(focus);
	
	$(edit_line).keypress(keyPressed);
	$(edit_line).focus(focus);
	
	return tmp;
}
